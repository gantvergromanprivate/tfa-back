
module.exports.sendResponse = function sendResponse(req, res, data) {
  if (data.success) {
    return res.send(data.data);
  }
  return res.status(data.code).send(data.userErrorMsg);
};
