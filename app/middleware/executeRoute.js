const { sendResponse } = require('./response');

async function aSyncExecuteRoute(req, res, f, validate) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Type of data
  // res.setHeader('Content-type', 'application/xml');

  const params = {
    params: req.params,
    query: req.query,
    body: req.body,
  };

  let result = { success: true };
  if (validate) {
    result = await validate(params);
  }

  if (result.success) {
    result = await f(params, res);
  }

  sendResponse(req, res, result);
}

module.exports.aSyncExecuteRoute = aSyncExecuteRoute;
