
module.exports = function errorResponse(err, req, res /* , next */) {
  let message = `>> URL:${req.url}`;
  message += `\n  ${err.message}\n  ${err.stack}`;
  console.error(message);

  if (err.expose && err.status && err.status >= 400) {
    // Should take care of express.json errors... and more
    res.status(err.status).send(message);
  } else {
    res.status(500).send('Error while handling the request.');
  }
};
