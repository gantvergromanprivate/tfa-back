const { Op } = require('sequelize');
const config = require('config');
const moment = require('moment');

const sequelize = require('../tools/sequelize');

const { fetchTweets } = require('../tools/twitter');
const { generateTweets } = require('../tools/mockup');
const { Word } = require('../schemas/db/word');

async function writeTweets(keyword, tweets) {
  const table = await Word(keyword);
  await table.bulkCreate(tweets);
}

async function cleanTable(keyword, from, to) {
  const table = await Word(keyword);
  await table.destroy({
    where: {
      createdAt: {
        [Op.gte]: from,
        [Op.lte]: to,
      },
    },
  });
}

async function mockupTweets({ body, query }) {
  const { keywords } = body;
  const {
    from,
    to,
    amount,
    clear,
  } = query;

  if (clear) {
    const promises = keywords.map(keyword => cleanTable(keyword, from, to));
    await Promise.all(promises);
  }

  keywords.map(keyword => generateTweets(
    {
      keyword,
      from,
      to,
      amount,
    },
    writeTweets,
  ));
  return {
    success: true,
    code: 202,
    data: 'Accepted succefffully',
  };
}

async function downloadTweets({ body, query }) {
  const { keywords } = body;
  const { from, to } = query;

  const promises = keywords.map(keyword => cleanTable(keyword, from, to));
  await Promise.all(promises);

  keywords.map(keyword => fetchTweets({ keyword, from, to }, writeTweets));
  return {
    success: true,
    code: 202,
    data: 'Accepted succefffully',
  };
}

function convertDate(date) {
  return moment(date).format('YYYY-MM-DD');
}

function queryText(db, alias, from, to) {
  return `
  SELECT
    strftime('%Y', createdAt) as year,
    strftime('%m', createdAt) as month,
    count(*) as count
  FROM ${alias}
  WHERE (createdAt>='${convertDate(from)}' and createdAt<='${convertDate(to)}')
  GROUP BY year, month;`;
}

async function requestAmount(db, alias, from, to) {
  const response = await sequelize.query(queryText(db, alias, from, to));
  return {
    alias,
    response,
  };
}

async function getSummary({ query }) {
  const { from, to } = query;
  let db = 'twitter';
  if (config.has('sql.db')) {
    db = config.get('sql.db');
  }

  const queryInterface = sequelize.getQueryInterface();
  const tables = await queryInterface.showAllSchemas();

  // mySQL
  // const aliases = tables.map(table => table[`Tables_in_${db}`]);
  // const promises = aliases.map(alias => requestAmount(db, alias, from, to));
  // sqlite
  const promises = tables.map(table => requestAmount(db, table, from, to));

  const responses = await Promise.all(promises);

  if (responses.length === 0) {
    return { success: false, code: 404, userErrorMsg: 'No data' };
  }
  const data = responses.reduce((accumulator, { alias, response }) => {
    const [summary] = response; // ToDo: research why lines are duplicates hese
    accumulator[alias] = summary;
    return accumulator;
  }, {});

  return { success: true, code: 200, data };
}

module.exports.mockupTweets = mockupTweets;
module.exports.downloadTweets = downloadTweets;
module.exports.getSummary = getSummary;
