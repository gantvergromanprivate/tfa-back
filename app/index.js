const express = require('express');

const app = express();

// For local DB storage
global.__basedir = process.cwd();

require('./startup/setMiddleware')(app);
require('./startup/db')();
require('./startup/routes')(app); // Must be after middleware. no routs/middleware should be created after this function
require('./startup/config')();

const port = process.env.PORT || 4001;
const server = app.listen(port, () => console.log(`API Server listening on port ${port}, Env: [${app.get('env')}]`));

// For running integration supertest from jest:
module.exports.server = server;
