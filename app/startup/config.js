const config = require('config');

function exitIfNotConfigure(key) {
  if (!config.get(key)) {
    throw new Error(`FATAL ERROR: ${key} is not defined.`);
  }
}
module.exports = function checkConfig() {
  // exitIfNotConfigure('twitter.consumer_key');
  // exitIfNotConfigure('twitter.consumer_secret');
  // exitIfNotConfigure('twitter.access_token_key');
  // exitIfNotConfigure('twitter.access_token_secret');
};
