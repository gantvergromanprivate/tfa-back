const error = require('../middleware/error');
const { twitterAPI, twitterAPIPath } = require('../routes/twitter');

module.exports = function setRoutes(app) {
  app.use(twitterAPIPath, twitterAPI);

  app.use((req, res /* , next */) => {
    const data = { URL: req.url, Method: req.method };
    res.status(404).send(JSON.stringify(data, null, 2));
  });

  app.use(error);
};
