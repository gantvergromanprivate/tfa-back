require('express-async-errors');
const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const addRequestId = require('express-request-id')();
const cors = require('cors');

module.exports = function initMW(app) {
  app.use(cors());
  app.use(addRequestId);

  app.use(helmet());
  app.use(compression());
  app.disable('etag'); // Solve the 304 on some URLs

  // Based on the Content-Type Header, parse the body.
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
};
