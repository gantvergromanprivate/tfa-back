const sequelize = require('../tools/sequelize');

module.exports = function setSequelize() {
  sequelize.sync()
    .then(() => console.log('DB was synchronized!'))
    .catch(error => console.log(error));
};
