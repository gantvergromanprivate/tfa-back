const Sequelize = require('sequelize');
const sequelize = require('../../tools/sequelize');

const Word = async (keyword) => {
  const Keyword = sequelize.define(
    keyword,
    {
      // id: {
      //   type: Sequelize.STRING(20),
      //   allowNull: false,
      //   primaryKey: true,
      //   autoIncrement: true,
      // },
      text: {
        type: Sequelize.STRING(160),
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    },
    {
      timestamps: false,
    },
  );
  await Keyword.sync();
  return Keyword;
};

module.exports.Word = Word;
