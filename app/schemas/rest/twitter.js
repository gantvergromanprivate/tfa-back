const Joi = require('joi');

function validatePeriod({ query }) {
  const schema = {
    query: {
      from: Joi.date().iso().required(),
      to: Joi.date().iso().required(),
    },
  };
  const { error } = Joi.validate({ query }, schema);
  if (error) return { success: false, code: 400, userErrorMsg: error.details[0].message };
  return {
    success: true, code: 200, userErrorMsg: '', data: '',
  };
}

function validateFetch({ body, query }) {
  const schema = {
    body: {
      keywords: Joi
        .array()
        .items(Joi.string())
        .min(1),
    },
    query: {
      from: Joi.date().iso(),
      to: Joi.date().iso(),
    },
  };
  const { error } = Joi.validate({ body, query }, schema);
  if (error) return { success: false, code: 400, userErrorMsg: error.details[0].message };
  return {
    success: true, code: 200, userErrorMsg: '', data: '',
  };
}

function validateMockup({ body, query }) {
  // TODO: refactor DRY
  const schema = {
    body: {
      keywords: Joi
        .array()
        .items(Joi.string())
        .min(1),
    },
    query: {
      from: Joi.date().iso(),
      to: Joi.date().iso(),
      amount: Joi.number(),
      clear: Joi.bool(),
    },
  };
  const { error } = Joi.validate({ body, query }, schema);
  if (error) return { success: false, code: 400, userErrorMsg: error.details[0].message };
  return {
    success: true, code: 200, userErrorMsg: '', data: '',
  };
}

function validateId(data) {
  const schema = {
    id: Joi.number().required(),
  };
  const { error } = Joi.validate(data.query, schema);
  if (error) return { success: false, code: 400, userErrorMsg: error.details[0].message };
  return {
    success: true, code: 200, userErrorMsg: '', data: '',
  };
}

module.exports.validatePeriod = validatePeriod;
module.exports.validateMockup = validateMockup;
module.exports.validateFetch = validateFetch;
module.exports.validateId = validateId;
