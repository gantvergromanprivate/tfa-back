const express = require('express');
const { aSyncExecuteRoute } = require('../middleware/executeRoute');

const router = express.Router({ mergeParams: true });
const basePath = '/';
const { mockupTweets, downloadTweets, getSummary } = require('../routesLogic/twitter');
const { validateMockup, validateFetch, validatePeriod } = require('../schemas/rest/twitter');

router.post('/mockup-tweets', async (req, res) => aSyncExecuteRoute(req, res, mockupTweets, validateMockup));
router.post('/fetch-tweets', async (req, res) => aSyncExecuteRoute(req, res, downloadTweets, validateFetch));
router.get('/tweet-report', async (req, res) => aSyncExecuteRoute(req, res, getSummary, validatePeriod));

module.exports.twitterAPI = router;
module.exports.twitterAPIPath = basePath;
