const faker = require('faker');

function addWord(str, injection) {
  const arr = str.split(' ');
  const pos = faker.random.number({ min: 0, max: arr.length });
  arr.splice(pos, 0, injection);
  return arr.join(' ');
}

function generateTweets(
  {
    keyword,
    from,
    to,
    amount = 1000,
  },
  callback,
) {
  const tweets = [];
  for (let i = 0; i < amount; i += 1) {
    tweets.push({
      createdAt: faker.date.between(from, to),
      // id: faker.random.word(),
      text: addWord(faker.lorem.words(5), keyword),
    });
  }
  callback(keyword, tweets);
}

module.exports.generateTweets = generateTweets;
