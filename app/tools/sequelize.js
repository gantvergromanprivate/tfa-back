const config = require('config');
const path = require('path');
const Sequelize = require('sequelize');

function sequelizeLocal() {
  let db = 'twitter';
  let user;
  let password;
  let options = {
    dialect: 'sqlite',
    storage: path.join(global.__basedir, 'db', 'twitter.db'),
  };
  if (config.has('sql.db')) {
    db = config.get('sql.db');
  }
  if (config.has('sql.user') && config.has('sql.password')) {
    user = config.get('sql.user');
    password = config.get('sql.password');
  }
  if (config.has('sql.options')) {
    const externalOptions = config.get('sql.options');
    let { dialectModulePath } = externalOptions;
    options = { ...options, ...externalOptions };
    if (dialectModulePath) {
      dialectModulePath = path.join(global.__basedir, dialectModulePath);
      options = { ...options, dialectModulePath };
    }
  }

  return new Sequelize(db, user, password, options);
}

function sequelizeProduction() {
  if (process.env.JAWSDB_MARIA_URL) {
    // the application is executed on Heroku ... use the MariaDB database
    let options = {
      dialect: 'sqlite',
    };
    if (config.has('sql.options')) {
      options = config.get('sql.options');
    }
    return new Sequelize(process.env.JAWSDB_MARIA_URL, options);
  }
  return sequelizeLocal();
}

function sequelize() {
  if (process.env.NODE_ENV === 'production') {
    return sequelizeProduction();
  }
  return sequelizeLocal();
}

module.exports = sequelize();
