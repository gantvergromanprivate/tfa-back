const axios = require('axios');
const config = require('config');
const moment = require('moment');

const instance = axios.create({
  baseURL: 'https://api.twitter.com/1.1/',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${config.get('twitter.bearer_token')}`,
  },
});

function parseTweets(tweetsRest) {
  const tweets = tweetsRest.map(({ created_at: createdAt, id, text }) => (
    {
      createdAt,
      id,
      text: text.trim().substring(0, 139),
    }
  ));
  return tweets;
}

function fetchTweets(
  {
    keyword,
    from,
    to,
    next = '',
  },
  callback,
  continueFetching = true,
) {
  if (!continueFetching) return;
  const requestData = {
    query: keyword,
    fromDate: moment(from).format('YYYYMMDDHHmm'),
    toDate: moment(to).format('YYYYMMDDHHmm'),
  };
  if (next) {
    requestData.next = next;
  }

  instance.post('tweets/search/fullarchive/development.json', requestData)
    .then(async ({ data }) => {
      const tweets = parseTweets(data.results);
      await callback(keyword, tweets);

      fetchTweets(
        {
          keyword,
          from,
          to,
          next: data.next,
        },
        callback,
        !!data.next,
      );
    })
    .catch(error => console.log(error));
}

module.exports.fetchTweets = fetchTweets;
