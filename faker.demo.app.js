const faker = require('faker');

function addWord(str, injection) {
  const arr = str.split(' ');
  const pos = faker.random.number({ min: 0, max: arr.length });
  arr.splice(pos, 0, injection);
  return arr.join(' ');
}
function fake(amount) {
  const result = [];
  for (let i = 0; i < amount; i += 1) {
    result.push({
      i: faker.random.number({ min: 0, max: 5 }),
      text: addWord(faker.lorem.words(5), 'hello'),
    });
  }
  return result;
}

console.log(fake(10));
