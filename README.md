# Test Assignment `Twitter Frequency Analysis` API Server (c) Roman Gantverg. 2019

Welcome to TFA API server.  

- [Test Assignment `Twitter Frequency Analysis` API Server (c) Roman Gantverg. 2019](#test-assignment-twitter-frequency-analysis-api-server-c-roman-gantverg-2019)
    - [Mockup Tweets](#mockup-tweets)
    - [Fetch Tweets](#fetch-tweets)
    - [Tweet Report](#tweet-report)
      - [Automatic install](#automatic-install)
      - [Manually install](#manually-install)
    - [SQLite installation](#sqlite-installation)
  - [TODO:](#todo)
      - [Twitter API payment account](#twitter-api-payment-account)
      - [SQLite use sqling3 package](#sqlite-use-sqling3-package)
      - [Tests](#tests)
      - [Docker](#docker)

##REST API endPoints
### Mockup Tweets
`/mockup-tweets?from=<date>&to=<date>&amount=<number>&clear=true|false`
body:
```
{
  keywords: [string]
}
```
Generate amount number of random tweets contains `keyword` between to dates. If clear===true clean previous DB with `keyword`.

### Fetch Tweets
`/fetch-tweets?from=<date>&to=<date>`
body:
```
{
  keywords: [string]
}
```
Fetch tweets using `Twitter API` between two dates.

### Tweet Report
`/tweet-report?from=<date>&to=<date>`

Respond with a json summary showing how many tweets each word got for every month

##Install Packages
#### Automatic install

(After creating the project folder from git)  
`cd $projectFolder`  
`npm outdated`  
`npm install`

#### Manually install

`sudo npm i -g nodemon`  
`npm init --save`  
`npm i express --save`  
`npm i express-async-errors --save`
`npm i helmet --save`
`npm i compression --save`
`npm i express-request-id --save`
`npm i cors --save`
`npm i sequelize --save`
`npm i sqlite3 --save`
`npm i config --save`
`npm i joi --save`
`npm i axios --save` 
`npm i moment --save`

##Install DB Environment

In the Assignment we Use SQLite DB. You might use the dialect that `Sequelize` support: SQLite, Postgree, MYSQL etc.
This Readme include SQLite instructions only. At the moment only `SQLite` dialect supported.

### SQLite installation
Download: https://www.sqlite.org/download.html
DBA Instruction: https://www.dev2qa.com/how-to-install-sqlite3-on-mac/


##Config
- All config files are on the `/config` folder
- `default.json` file include all the common values that are the same regardless the environment.  
  Environment files will **add** to the default config
- File to **each environment**.  
  File name is as the name of the environment (same as the value of the `NODE_ENV` environment variable)  
  We should have `production.json`, `test.json`, `development.json`
- **No** sensitive information (passwords, tokens) should be stored in the config files!  
  All **sensitive information** must be stored in **environment variables**.
- The config file with the name `custom-environment-variables.json` is used to map values from the environment variables to the config JSON object

###Twitter Configuration.
All twitter credentionals shoul be saved secure in env variables.
We use `Bearer Token` authorizaition (https://developer.twitter.com/en/docs/basics/authentication/guides/bearer-tokens)
```
{
  "twitter": {
    "bearer_token": "TWITTER_BEARER_TOKEN"  
  } 
}
```

###DB Configuration.
Application use `Sequelize` ORM. It's possible to config it using next variables:
```
  "sql" : {
    "db": <any name>,
    "user": <username>,
    "password": <password>
    "options": {
      "dialect": "sqlite",
      "storage": "./db/twitter.sqlite"
    }
  }
```
`options` variable might contain any of Sequelize options.


## TODO:
#### Twitter API payment account
In the `Sandbox` we might make only 50 requests per month

#### SQLite use sqling3 package

#### Tests

#### Docker