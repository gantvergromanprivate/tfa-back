/* eslint-disable global-require */
const request = require('supertest');
const moment = require('moment');

global.__basedir = process.cwd();
const sequelize = require('../../app/tools/sequelize');
const { Word } = require('../../app/schemas/db/word');

const KEYWORD = 'ThisFraseNeveBeenUsedInTheSearch';
const KEYWORD_PLURAL = 'ThisFraseNeveBeenUsedInTheSearches';

let server;

beforeAll(async () => {
  // eslint-disable-next-line prefer-destructuring
  server = require('../../app/index').server;

  const queryInterface = sequelize.getQueryInterface();
  await queryInterface.dropTable(KEYWORD_PLURAL);
});

afterAll(async () => {
  const queryInterface = sequelize.getQueryInterface();
  await queryInterface.dropTable(KEYWORD_PLURAL);

  await server.close();
});

describe('/tweet-report [GET] REST Endpoints', () => {
  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    server = require('../../app/index').server;
    const queryInterface = sequelize.getQueryInterface();
    await queryInterface.dropTable(KEYWORD_PLURAL);
  });

  afterEach(async () => {
    const queryInterface = sequelize.getQueryInterface();
    await queryInterface.dropTable(KEYWORD_PLURAL);

    await server.close();
  });

  it('should return 400 if parameters are not in the correct structure', async () => {
    const res = await request(server)
      .get('/tweet-report');
    expect(res.status).toBe(400);
  });

  it('should return 400 if query is not in the correct structure', async () => {
    const res = await request(server)
      .get('/tweet-report?from=this_is_not_a_date&to=2018-04-01T23:59:59');
    expect(res.status).toBe(400);
  });

  it('should return id if everything is OK', async () => {
    const table = await Word(KEYWORD);
    const curDate = '2018-01-02T01:00:00+00:00';
    await table.create({
      id: '12345678901234567890',
      createdAt: curDate,
      text: 'Any text',
    });
    const from = moment(curDate).subtract(1, 'months').toISOString();
    const to = moment(curDate).add(1, 'months').toISOString();

    const res = await request(server)
      .get(`/tweet-report?from=${from}&to=${to}`);
    expect(res.status).toBe(200);

    const data = JSON.parse(res.text);
    const expected = {};
    expected[KEYWORD_PLURAL] = [{
      year: 2018,
      month: 1,
      count: 1,
    }];
    expect(data).toMatchObject(expected);
  });
});
