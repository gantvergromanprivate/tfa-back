SELECT name FROM sqlite_master WHERE type='table';
SELECT * FROM 'managers';
SELECT
  strftime('%Y', createdAt) as year,
  strftime('%m', createdAt) as month,
  count(*) as count
FROM managers
WHERE (createdAt>='2019-01-01' and createdAt<='2019-04-01')
GROUP BY year, month;
