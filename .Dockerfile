FROM node

LABEL authors="Roman Gantverg"

WORKDIR /usr/app/tfa-back

COPY ./package.json ./
RUN npm install
COPY ./ ./

CMD ["npm", "start"]